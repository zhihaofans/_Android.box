### 现有功能：

一. 二维码生成与解析。暂不支持选择图片解析二维码，用的是这个二维码库 [[XQRCode](https://github.com/xuexiangjys/XQRCode)]。

二. Android SDK版本列表查看。最高支持 Android 8.1 (API 27, Oreo 奥利奥)，同时还显示当前设备的版本。

三. 应用管理。现在只支持查看应用名称、包名、版本、安装包路径、安装包大小、安装时间、最后更新时间。

四. 新闻盒子。暂不支持登陆，现在支持以下站点及频道：

1. [少数派](https://www.sspai.com/) :官网抓取其api来用，json格式。

- 少数派文章

2. [数字尾巴](http://www.dgtle.com/) :官网抓取其api来用，json格式。

- [数字尾巴鲸闻](http://news.dgtle.com/)

3. [干货集中营](http://gank.io/) :[官方提供api](http://gank.io/api)，json格式。

- 全部

- 安卓

- 福利

4. [品玩](http://www.pingwest.com/) :官网抓取其api来用，json格式。

- 转发推荐

5. [全球聚合](http://all.gl/) :官网抓取其网页来解析，html格式。

- 全部

6. [观点](http://www.guandn.com/) :官网抓取其api来用，json格式。

- 首页

7. [RSSHub](https://docs.rsshub.app/) :官方提供api，json格式。

- [我以为我很浮夸的微博](https://weibo.com/u/6180475384)

- [V2EX-最新主题](https://www.v2ex.com)

- [豆瓣正在上映的电影](https://movie.douban.com/cinema/nowplaying/guangzhou/)

五. 天气。只支持 **当前IP** 的地名、当前气候、当前气温。

六. 哔哩哔哩。

1. 通过弹幕列表里指定弹幕的加密用户id得知正式用户id。有时可能会得到多个结果。

七. [Server酱](http://sc.ftqq.com/3.version) :可以推送消息至微信公众号。
